# Project Title

Basic Project implemeting functionalities like POST,GET and PUT HttpResponse in Django-Python.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Following tools and languages should be installed before running this project:  
- Django 1.9  
- ipython 5.5.0  
- virtualenv 15.1.0  
- pip 9.0.1  
- Postman  

### Installing

Following steps should be followed for installation:  
- Install pip by   
	sudo easy_install pip  
- Use following command to install django   
	sudo pip install django==1.9  
- Use following command to install ipython  
	sudo pip install ipython  
- Use following command to install virtual env  
	pip install virtualenv  

## Running the tests

To run the project, following commands should be used in project root directory:  
- python manage.py makemigrations MidTerm  
- python manage.py migrate  
- python manage.py runserver  
After these commands use Postman for different HTTP requests.

