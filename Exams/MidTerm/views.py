from django.shortcuts import render
from django.http import HttpResponse
from MidTerm.models import Question , Student
from django.views.generic import View
from django.core import serializers
from django.http import QueryDict

import json

# Create your views here.
class Queries_on_Question(View):
    '''
        This class is used to query for following purposes:
        - Querying on question table
        - Inserting values in Question table if not present else updates
        - Updating values of foreign key
    '''
    def get(self,request):
        '''
            This method provide features like all(),filter(),exclude() and get() on the basis of id argument.
        '''
        query_type = request.GET
        query = Question.objects.all()
        if query_type.get('id') == '1':
            query = Question.objects.all()
        elif query_type.get('id') == '2':
            query = Question.objects.filter(question_id = 2)
        elif query_type.get('id') == '3':
            query = Question.objects.exclude(question_id = 3)
        elif query_type.get('id') == '4':
            query = Question.objects.get(question_id = 4)
        data = [] 
        for each_row in query:
            data.append({
                'question_text' : each_row.question_text,
                'user_answer' : each_row.user_answer,
                'question_id' : each_row.question_id,
                })
        return HttpResponse(json.dumps(data),content_type = 'application/json')

    def post(self,request):
        '''
            This method is used to fill table Question if it does not exist
            else it updates
        '''
        data = request.POST
        question_text = data.get('question_text')
        user_answer = data.get('user_answer')
        qid = data.get('question_id')
        try:
            question_object = Question.objects.get(question_id = qid)
            question_object.question_text = question_text
            question_object.user_answer =  user_answer
            question_object.question_id = qid
            question_object.save()
            return HttpResponse("Successfully Updated!!")
        except:
            new_data = Question(question_text = question_text,user_answer = user_answer,question_id = qid)
            new_data.save()
            return HttpResponse("Successfully Added!!")

    def put(self,request):
        '''
            This method updates the values of both Question and Student table simultaneously by taking suitable parameters
        '''
        #import pdb;pdb.set_trace()
        data = QueryDict(request.body)
        question_text = data.get('question_text')
        user_answer = data.get('user_answer')
        qid = data.get('question_id')
        roll_no = data.get('roll_no')
        student_name = data.get('student_name')
        try:
            question_object = Question.objects.get(question_id = qid)
            student_object = Student.objects.get(question_id = qid)
            student_object.roll_no = roll_no
            student_object.student_name = student_name
            student_object.save()
            question_object.question_text = question_text
            question_object.user_answer =  user_answer
            question_object.question_id = qid
            question_object.save()
            return HttpResponse("Successfully Updated!!")
        except Question.DoesNotExist:
            print("The requested record does not exist")  

        return HttpResponse("Unknown Response!!")


class Queries_on_Student(View):
    '''
        This class can be used for following:-
        - To display all records of Student Table
        - To insert recors in Student table if it does not exist
        - To query on Student Table
    '''
    def get(self,request):
        '''
            This method displays all records of Student table.
        '''
        #Student.objects.filter(pk=2).delete()
        query = Student.objects.all()
        data = [] 

        for each_row in query:
            data.append({
                'roll_no' : each_row.roll_no,
                'question_id' : serializers.serialize('json', [each_row.question_id, ]),
                'student_name' : each_row.student_name,
                })
        return HttpResponse(json.dumps(data),content_type = 'application/json')

    def post(self,request):
        '''
            This method is used to fill table Student if it does not exist
            else it updates
        '''
        data = request.POST
        roll_no = data.get('roll_no')
        student_name = data.get('student_name')
        qid = data.get('question_id')
        question_text = data.get('question_text')
        user_answer = data.get('user_answer')
        #question_object = Question.objects.get(question_id = qid)
        try:
            student_object = Student.objects.get(question_id = qid)
            student_object.roll_no = roll_no
            student_object.student_name =  student_name
            student_object.question_id = qid
            student_object.save()
            return HttpResponse("Successfully Updated!!")
        except:
            new_ques = Question(question_text = question_text,question_id = qid,user_answer = user_answer)
            new_ques.save()
            new_data = Student(roll_no = roll_no,student_name = student_name,question_id = new_ques)
            new_data.save()
            return HttpResponse("Successfully Added!!")
