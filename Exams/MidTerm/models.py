from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Question(models.Model):
	'''
		Question model class describe Table Question with question_text,user_answer and question_id as its column fields
	'''
	LENGTH = 200 #Constant Field
	question_text = models.CharField(max_length=LENGTH)
	user_answer = models.CharField(max_length=LENGTH)
	question_id = models.IntegerField(primary_key=True)

	def __unicode__(self):
		return str(self.question_id) + ' ' + str(self.question_text) + ' ' + str(self.user_answer) + '\n'

class Student(models.Model):
	'''
		Student model class describe Table Student with roll_no,question_id and student_name as its column field
	'''
	LENGTH = 200 #Constant Field
	roll_no = models.IntegerField(default=0)
	question_id = models.ForeignKey(Question,on_delete=models.CASCADE)
	student_name = models.CharField(max_length=LENGTH)

	def __unicode__(self):
		return str(self.roll_no) + ' ' + str(self.question_id) + ' ' + str(self.student_name) + '\n'