
from django.conf.urls import url
from MidTerm.views import Queries_on_Question,Queries_on_Student
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^midterm/queryques/$',csrf_exempt(Queries_on_Question.as_view())),
    url(r'^midterm/querystud/$',csrf_exempt(Queries_on_Student.as_view())),
]